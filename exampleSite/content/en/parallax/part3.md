---
title: "Find us in Bouaké !"
date: 2020-07-09T16:41:42Z
draft: false
weight: 30
---

We are easy to locate.

Click on the button below for directions from Google Maps.

or:

Take the road towards Belleville.  We are at Belleville 1, at the old commisariat intersection. (carréfour de l'ancien commissariat)


